/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gd.utils;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;

/**
 *
 * @author Julio
 */
public class Time {
    
    private static DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    public static String getTimeAsString() {
        try {
            return getInetTimeAsString();
        } catch (Exception ex) {
            return getSystemTimeAsString();
        }
    }
    
    public static String getTimeAsString(DateFormat df) {
        try {
            return getInetTimeAsString(df);
        } catch (Exception ex) {
            return getSystemTimeAsString(df);
        }
    }
    
    public static Date getTimeAsDate() {
        try {
            return getInetTimeAsDate();
        } catch (Exception ex) {
            return getSystemTimeAsDate();
        }
    }
    
    public static String getSystemTimeAsString() {
       Date dateobj = new Date();
       return (df.format(dateobj));       
    }
    
    public static String getSystemTimeAsString(DateFormat df) {
       Date dateobj = new Date();
       return (df.format(dateobj));       
    }
    
    public static Date getSystemTimeAsDate() {
       return new Date();
    }
    
    public static String getInetTimeAsString() throws UnknownHostException, IOException {
        String TIME_SERVER = "time.nist.gov";   
        NTPUDPClient timeClient = new NTPUDPClient();
        InetAddress inetAddress = InetAddress.getByName(TIME_SERVER);
        TimeInfo timeInfo = timeClient.getTime(inetAddress);
        long returnTime = timeInfo.getMessage().getTransmitTimeStamp().getTime();
        Date time = new Date(returnTime);
        return df.format(time);
    }
    
    public static String getInetTimeAsString(DateFormat df) throws UnknownHostException, IOException {
        String TIME_SERVER = "time.nist.gov";   
        NTPUDPClient timeClient = new NTPUDPClient();
        InetAddress inetAddress = InetAddress.getByName(TIME_SERVER);
        TimeInfo timeInfo = timeClient.getTime(inetAddress);
        long returnTime = timeInfo.getMessage().getTransmitTimeStamp().getTime();
        Date time = new Date(returnTime);
        return df.format(time);
    }
    
    public static Date getInetTimeAsDate() throws Exception {
        String TIME_SERVER = "time.nist.gov";   
        NTPUDPClient timeClient = new NTPUDPClient();
        InetAddress inetAddress = InetAddress.getByName(TIME_SERVER);
        TimeInfo timeInfo = timeClient.getTime(inetAddress);
        long returnTime = timeInfo.getMessage().getTransmitTimeStamp().getTime();
        return new Date(returnTime);
    }
    
}